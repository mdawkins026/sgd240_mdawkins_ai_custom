# Spike Plan

# Genetic algorithm machine learning in ue4

## Context

The aim of this spike is to have an ai learn or improve over time when playing again the player in a simple scenario. This is advantageous in creating truly intelligent npcs and ai characters that can adapt to an environment.

## Gap

The team has little to know knowledge of machine learning and the more specifically the genetic algorithm. We need to learn:

1. Understand: the genetic algorithm and how it works
2. Skill: how to translate this theory into a working model that can be implemented in ue4
3. Knowledge and ability: how does this perform and what are the limitations of this form of machine learning in ue4.

## Goals/Deliverables

The deliverables of this spike are as follows:

1. To understand the genetic algorithm model for machine learning
2. To implement said model in a ue4 project that is simple and with the scope time frame
3. To complete this code in c++ and to enable expansions at a later date

## Dates

| Planned start date: |  27/09/2019 |
| --- | --- |
| Deadline: | 26/10/2019 |

## Planning Notes

1. Research and gain understanding of the algorithm to be used
2. Research basic implementations in python
3. Design a simple project that can be accomplished within the scope of this spike
4. Complete said project
5. Debug and gauge effectiveness