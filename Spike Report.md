# Spike Report

# Genetic algorithm machine learning in ue4

## Introduction

This spike aims to generate knowledge on and test the viability of generic algorithm machine learning in ue4 to create ai that learns to play against the player. This is highly experimental and thus some bugs may be experienced.

This will be accomplished by delivering on the spike plan deliverables on time and within scope.

## Goals

1. To deliver on the spike deliverables as per the spike plan.
2. To gain an understanding of machine learning specifically:
  1. How it is implemented
  2. How it can be used in ue4
3. To gauge the effectiveness of basic machine learning in games
4. To stay within the scope of this spike as per the spike plan.

| Primary – Matthew Dawkins | Secondary – N/A |
| --- | --- |

## Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

- Genetic algorithm explanation (with code in java) [https://towardsdatascience.com/introduction-to-genetic-algorithms-including-example-code-e396e98d8bf3](https://towardsdatascience.com/introduction-to-genetic-algorithms-including-example-code-e396e98d8bf3)
- Genetic algorithm explanation [https://medium.com/analytics-vidhya/understanding-genetic-algorithms-in-the-artificial-intelligence-spectrum-7021b7cc25e7](https://medium.com/analytics-vidhya/understanding-genetic-algorithms-in-the-artificial-intelligence-spectrum-7021b7cc25e7)
- Beginners guide [https://skymind.ai/wiki/evolutionary-genetic-algorithm](https://skymind.ai/wiki/evolutionary-genetic-algorithm)
- Python example [https://towardsdatascience.com/genetic-algorithm-implementation-in-python-5ab67bb124a6](https://towardsdatascience.com/genetic-algorithm-implementation-in-python-5ab67bb124a6)
- First video in tutorial series [https://www.youtube.com/watch?v=9zfeTw-uFCw](https://www.youtube.com/watch?v=9zfeTw-uFCw)
- Machine learning blog on this topic [https://www.neuraldesigner.com/blog/genetic\_algorithms\_for\_feature\_selection](https://www.neuraldesigner.com/blog/genetic_algorithms_for_feature_selection)
- Cross over [https://en.wikipedia.org/wiki/Crossover\_(genetic\_algorithm)](https://en.wikipedia.org/wiki/Crossover_(genetic_algorithm))
- Further on cross over [https://medium.com/@becmjo/to-be-completed-c5ace77bba88](https://medium.com/@becmjo/to-be-completed-c5ace77bba88)
- Normal distribution noise func in c++ [https://en.cppreference.com/w/cpp/numeric/random/normal\_distribution](https://en.cppreference.com/w/cpp/numeric/random/normal_distribution)
- FMath api notes [https://docs.unrealengine.com/en-US/API/Runtime/Core/Math/FMath/index.html](https://docs.unrealengine.com/en-US/API/Runtime/Core/Math/FMath/index.html)

## Tasks undertaken

Before any learning can take place, first a scenario had to be produced where this sort of machine learning might work. In this case a player must stop an ai from firing balls into a goal behind it. The ai can only move left and right as can the player.

To complete this, we must first create our ai, player, ball and goal:

- The player is a fairly simple character that can only move left to right.
- The ai or bot can fire balls, these balls are a projectile and have the associated motion and are destroyed if they hit the player
- The goal is a simple trigger box that updates the score of each bot based on how many ball the bot gets in the goal (this acts as a simple fitness function).

As for the bot itself, this is slightly more complicated. Firstly we must define how our bot will decide where and when to fire each ball. To make this seem random we can implement a normal noise function and then use the mean and standard distribution values as the values that will be optimized. Thus, for each of the following we have a mean and standard deviation value

- Time in between shot
- Position in the x direction
- Force at which the ball is propelled (r value)
- The direction of the ball (theta value)

Then we implement a loop function that goes through each shot and then set the state of the bot to inactive (this will be used by our learning object). In this example the loop is as follows

![](reportImages\1.png)

Note that the timers refer the wait time in between shots and then at the end of the bot&#39;s life.

Now that we have our bot, we need to have an object that first runs each generation, then from that generation selects the two best performing bots and performs cross over to get a new generation. Then repeats the process.

Breaking this down we first need a way to represent bot of the generation in terms of data, for this we can use a struct, which in this example is as follows

![](reportImages\2.png)

Then with a method for populating random data (this is how any random bot is produced).

![](reportImages\3.png)

Then we need this managing object or bot manager to create an initial or seed generation, to do this we can implement the following in the cpp file with the associated variables in the header

![](reportImages\4.png)

Then we need to model the algorithm in our code, to do this, this example implements the following loop.

![](reportImages\5.png)

Which is based on the following diagram

![](reportImages\6.png)

Given the complexity of many of the methods in the main loop, this report will only cover the functionality and not the inner workings.

**RunGeneration**

This is where the main running of the game takes place and acts as the fitness assessment

**GenerateNextGeneration**

this function generates the next evolution the generation and handles the selection, crossover and mutation of the previous generation to get a new one.

## What we found out

Throughout this spike major lessons were learnt in how to implement machine learning algorithms in ue4. It was found that the algorithm under most circumstances improved its performance against the player although not quite as expected. Overall some specific areas of knowledge include

- Fitness functions and assessments
- Selections crossover and creating new generations
- Designing implementations that can use this style of machine learning

Although this was an interesting area of research/ application the results do not match up to the already existing ue4 ai mechanics. This pitfall will be explained in a further section however the ai and situation it is placed in either struggles to learn or learns too quickly and is impossible to beat. From a game play perspective this is not ideal as the experience from a player&#39;s point of view is either boring or impossible.

Overall this idea was an interesting concept however it is either impractical or will require serous amount of further research and/or reworking to become useful in game.

## Open Issues/risks

There is currently a bug in the code that causes a ue4 editor crash due to an array index out of bounds error. Due to the scope of this project, specifically time restraints this bug is yet to be fixed.

Also there is considerable limitation to the effectiveness of this implantation and this will be reflected in the learning of the bots. Specifically, the point at which the bots do not get any better. This is most likely due to two reasons.

- Error in the algorithm
- Un optimized learning parameters

## Recommendations

It is recommended that further spikes be conducted in the following

- Improving the implementation of genetic algorithm in ue4
- Using the tensorflow plugin to apply complex machine learning to ue4
- Implementing other types of machine learning in ue4