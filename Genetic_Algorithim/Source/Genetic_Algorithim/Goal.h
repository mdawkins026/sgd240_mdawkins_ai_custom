// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "Goal.generated.h"

/**
 * 
 */
UCLASS()
class GENETIC_ALGORITHIM_API AGoal : public ATriggerBox
{
	GENERATED_BODY()
	
};
