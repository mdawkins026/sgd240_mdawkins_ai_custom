// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BotManager.generated.h"

USTRUCT()
struct FBotData
{
	GENERATED_BODY()

	//Constructor
	FBotData()
	{
		// Always initialize your USTRUCT variables!
		// Exception is if you know the variable type has its own default constructor
	}

public:
	// Mean data
	// Structure {time, x position, r, theta)
	float Averages[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	// Standard Deviation data
	// Structure {time, x position, r, theta)
	float StandardDeviations[4] = { 0.0f,0.0f,0.0f,0.0f };

	// Max Varience from the mean (Must be less then 1)
	float maxVarience = 0.35;

	// Mean Time Bounds
	float MeanTimeBounds[2] = { 0.0f, 2.5f };

	// Mean X Position Bounds 
	float MeanPositionBounds[2] = { -100.0f, 100.0f };

	// Mean R (force) Bounds
	float MeanForceBounds[2] = { 750.0f, 2000.0f };

	void populateRand() 
	{
		if (maxVarience <= 1 && maxVarience >= 0) 
		{
			// Populate time data
			Averages[0] = FMath::RandRange(MeanTimeBounds[0], MeanTimeBounds[1]);
			float Lower = (maxVarience*Averages[0]) - Averages[0];
			float upper = (maxVarience*Averages[0]) + Averages[0];
			StandardDeviations[0] = FMath::RandRange(Lower, upper);

			// Populate XPos data
			Averages[1] = FMath::RandRange(MeanPositionBounds[0], MeanPositionBounds[1]);
			Lower = (maxVarience*Averages[1]) - Averages[1];
			upper = (maxVarience*Averages[1]) + Averages[1];
			StandardDeviations[1] = FMath::RandRange(Lower, upper);

			// Populate Rforce Data
			Averages[2] = FMath::RandRange(MeanForceBounds[0], MeanForceBounds[1]);
			Lower = (maxVarience*Averages[2]) - Averages[2];
			upper = (maxVarience*Averages[2]) + Averages[2];
			StandardDeviations[2] = FMath::RandRange(Lower, upper);

			// Populate Direction Data
			Averages[3] = FMath::RandRange(-90, 90);
			Lower = (maxVarience*Averages[3]) - Averages[3];
			upper = (maxVarience*Averages[3]) + Averages[3];
			StandardDeviations[3] = FMath::RandRange(Lower, upper);
		}

	}
};

UCLASS()
class GENETIC_ALGORITHIM_API ABotManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABotManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// The Generation number
	int generationNumber = 0;

	// The Size of the generation 
	int GenerationSize = 5;

	// Current bot in generation
	int Current = 0;

	// Number of steps
	int NumberOfSteps = 5;

	// The variation amount for each generation
	float GenerationVariation = 0.2;

	// Current Bot Number In Generation
	int CurrentBotNumberInGeneration = 0;

	bool GenerationRunning = false;

	TArray<FBotData> CurrentGeneration;

	TArray<int> indexOfPerents;

	TArray<int> CurrentScores;

	class ABot* CurrentActiveBot;

	void GenerateSeedGeneration();

	FBotData GenerateRandomBot();

	void GenerateNextGeneration();

	void RunGeneration();

	void CreateAndSpawn(int index);

	void Main();

	FBotData CrossOver(FBotData A, FBotData B);
};
