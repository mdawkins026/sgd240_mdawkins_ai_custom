// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BotController.generated.h"

/**
 * 
 */
UCLASS()
class GENETIC_ALGORITHIM_API ABotController : public AAIController
{
	GENERATED_BODY()
	

public:
	virtual void Tick(float DeltaTime) override;

	virtual void OnPossess(APawn* InPawn) override;

	bool isWaiting = false;

	// Timming
	FTimerHandle PauseTimmerHandle;
	
	UFUNCTION()
	void Continue();
};
