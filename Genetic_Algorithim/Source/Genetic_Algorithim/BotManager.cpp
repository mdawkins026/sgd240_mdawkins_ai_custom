// Fill out your copyright notice in the Description page of Project Settings.


#include "BotManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/World.h"
#include "Bot.h"
#include "Math/UnrealMathUtility.h"
#include <random>
#include "EngineGlobals.h "

// Sets default values
ABotManager::ABotManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABotManager::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("seed generated "));
	GenerateSeedGeneration();
}

// Called every frame
void ABotManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Main loop
	Main();

	UE_LOG(LogTemp, Warning, TEXT("Current generation is %d "), generationNumber);
	UE_LOG(LogTemp, Warning, TEXT("Current Bot Number is %d"), CurrentBotNumberInGeneration);
	UE_LOG(LogTemp, Warning, TEXT("length of the bot array %d"), CurrentGeneration.Num());
}

void ABotManager::GenerateSeedGeneration()
{
	for (int i = 0; i < GenerationSize; i++) 
	{
		CurrentGeneration.Add(GenerateRandomBot());
	}
}

FBotData ABotManager::GenerateRandomBot()
{
	FBotData NewData = FBotData();
	NewData.populateRand();
	return NewData;
}

void ABotManager::GenerateNextGeneration()
{
	// createas a new generation from the best two bots, with randomness as well a creating x new random bot
	
	TArray<int> indexOfPerents;

	FBotData PerantA;
	FBotData PerantB;
	
	// Get the heighest score
	int max = CurrentScores[0];
	for (int i = 0; i < CurrentScores.Num(); i++) 
	{
		if (CurrentScores[i] > max) 
		{
			max = CurrentScores[i];
		}
	}

	// Get the two heighest scores
	TArray<int> EqualToMax;
	TArray<int> EqualToSecoundHeighest;

	for (int i = 0; i < CurrentScores.Num(); i++)
	{
		if (CurrentScores[i] == max) 
		{
			EqualToMax.Add(i);
		}
	}

	// if there is only one of the heighest scores
	if (EqualToMax.Num() == 1) 
	{
		UE_LOG(LogTemp, Warning, TEXT("Firing only one heighest score"));
		int newint = 0;
		// find secound heigest score
		for (int i = 0; i < CurrentScores.Num(); i++)
		{
			if (CurrentScores[i] < max && CurrentScores[i] > newint) 
			{
				newint = CurrentScores[i];
			}

			// commented out for debuging
			/*if (newint == int() && CurrentScores[i] < max)
			{
				newint = CurrentScores[i];
			}

			else if (CurrentScores[i] > newint && CurrentScores[i] < max) 
			{
				newint = CurrentScores[i];
			}*/
		}

		// Populate the secound heighest score
		for (int i = 0; i < CurrentScores.Num(); i++) 
		{
			if (CurrentScores[i] == newint) 
			{
				EqualToSecoundHeighest.Add(i);
			}
		}

		// Select the heighest values and one of the secound heighest to be perents a and b
		PerantA = CurrentGeneration[EqualToMax[0]];

		PerantB = CurrentGeneration[EqualToSecoundHeighest[FMath::RandRange(0, EqualToSecoundHeighest.Num()-1)]];

	}

	// Select two of the highest value set to be the perents a and b
	else 
	{
		UE_LOG(LogTemp, Warning, TEXT("Firing only multi heighest score"));
		// select two random index from the equal to max array
		// first perent 
		int newF = FMath::RandRange(0, EqualToMax.Num() - 1);
		// secound perent
		int newS = FMath::RandRange(0, EqualToMax.Num() - 1);

		// if both random values are the same then generate a new one
		if (newF == newS) 
		{
			while (newF == newS)
			{
				newS = FMath::RandRange(0, EqualToMax.Num() - 1);
			}
		}
		
		PerantA = CurrentGeneration[newF];
		PerantB = CurrentGeneration[newS];
	}
	// perform crossover for all members of the new generation-1
	// clear the current array
	CurrentGeneration = TArray<FBotData>();

	// create the children
	for (int i = 0; i <= GenerationSize-2; i++) 
	{
		UE_LOG(LogTemp, Warning, TEXT("New child is created"));
		CurrentGeneration.Add(CrossOver(PerantA, PerantB));
	}

	// add one random member to the generation
	UE_LOG(LogTemp, Warning, TEXT("New child is created"));
	FBotData RandChildA = FBotData();
	RandChildA.populateRand();
	CurrentGeneration.Add(RandChildA);
}

void ABotManager::RunGeneration()
{
	UE_LOG(LogTemp, Warning, TEXT("Current generation.NUM() is %d "), CurrentGeneration.Num());

	// Must set generation running to false when finished
	// Base case 
	if (CurrentActiveBot == nullptr) 
	{
		UE_LOG(LogTemp, Warning, TEXT("first case called"));
		CreateAndSpawn(CurrentBotNumberInGeneration);
		CurrentBotNumberInGeneration++;
	}

	// All other cases
	else if (CurrentActiveBot->Active == false && CurrentBotNumberInGeneration < CurrentGeneration.Num())
	{
		UE_LOG(LogTemp, Warning, TEXT("Other case called"));
		// get the score of the bot
		CurrentScores.Add(CurrentActiveBot->score);
		// Getspawn the bot
		CurrentActiveBot->Destroy();
		// update to the next bot in the generation
		CreateAndSpawn(CurrentBotNumberInGeneration);
		CurrentBotNumberInGeneration++;
	}

	// End case
	else if (CurrentActiveBot->Active == false) 
	{
		UE_LOG(LogTemp, Warning, TEXT("End case called"));
		// get the score of the bot
		CurrentScores.Add(CurrentActiveBot->score);
		// Getspawn the bot
		CurrentActiveBot->Destroy();

		// Reset the generation bot number
		CurrentBotNumberInGeneration = 0;
		// the generation is no longer running as it is false;
		GenerationRunning = false;
	}	
}

void ABotManager::CreateAndSpawn(int index)
{
	// Attempt to fire a projectile.
	UWorld* World = GetWorld();
	if (World)
	{
		// Spawn the projectile at the muzzle.
		ABot* NewBot = World->SpawnActor<ABot>(this->GetActorLocation(), this->GetActorRotation());
		if (NewBot)
		{
			std::random_device rd;
			std::mt19937 gen{ rd() };

			UE_LOG(LogTemp, Warning, TEXT("Bot Spawned"));

			// generate gausuan noise values for all bot paramiters
			for (int i = 0; i < NumberOfSteps; i++) 
			{
				UE_LOG(LogTemp, Warning, TEXT("Fire position added "));

				// Generate and add timing values
				std::normal_distribution<float> valueTime{ CurrentGeneration[index].Averages[0], CurrentGeneration[index].StandardDeviations[0] };
				NewBot->TimmingValues.Add(valueTime(gen));

				// Generate and add position values 
				std::normal_distribution<float> valuePosition{ CurrentGeneration[index].Averages[1], CurrentGeneration[index].StandardDeviations[1] };
				NewBot->PositionValues.Add(valuePosition(gen));

				// Generate and add Rvalues 
				std::normal_distribution<float> valueR{ CurrentGeneration[index].Averages[2], CurrentGeneration[index].StandardDeviations[2] };
				NewBot->Rvalues.Add(valueR(gen));

				// Generate and add theta values
				std::normal_distribution<float> valueTheta{ CurrentGeneration[index].Averages[3], CurrentGeneration[index].StandardDeviations[3] };
				NewBot->ThetaValues.Add(valueTheta(gen));
			}
			CurrentActiveBot = NewBot;
		}
	}
}

void ABotManager::Main()
{
	// if there is a generation loaded and it has no data yet recorded start running the generation
	if (GenerationRunning == false && CurrentScores.Num() == 0) 
	{
		UE_LOG(LogTemp, Warning, TEXT("generation needs testing !"));
		// Try the generation
		GenerationRunning = true;
	}

	// If the generation is set to run the run the genration loop
	else if (GenerationRunning == true) 
	{
		UE_LOG(LogTemp, Warning, TEXT("Running generation !"));
		RunGeneration();
	}

	// if there is a generation loaded 
	else if (GenerationRunning == false && CurrentScores.Num() != 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("generation needs to learn !"));
		// Learn
		GenerateNextGeneration();
		generationNumber++;
		GenerationRunning = true;
		CurrentScores.Empty();
	}
}

FBotData ABotManager::CrossOver(FBotData A, FBotData B)
{
	int crossoverPointAverages = FMath::RandRange(0, 3);
	int crossoverPointSD = FMath::RandRange(0, 3);

	FBotData child = FBotData();

	float childAV[4];

	float childSD[4];

	// for Averages
	// add the elemnts upto and including the cross over index
	for (int i = 0; i <= crossoverPointAverages; i++) 
	{
		childAV[i] = A.Averages[i];
	}

	// if the cross over index is not the max itterator, add the elements after
	if (crossoverPointAverages != 3) 
	{
		for (int i = crossoverPointAverages+1; i <= 3; i++)
		{
			childAV[i] = A.Averages[i];
		}
	}

	// for SD
	// add the elemnts upto and including the cross over index
	for (int i = 0; i <= crossoverPointSD; i++)
	{
		childSD[i] = A.StandardDeviations[i];
	}

	// if the cross over index is not the max itterator, add the elements after
	if (crossoverPointSD != 3)
	{
		for (int i = crossoverPointSD + 1; i <= 3; i++)
		{
			childSD[i] = A.StandardDeviations[i];
		}
	}

	// write the crossover to the new child
	for (int i = 0; i <= 3; i++) 
	{
		child.Averages[i] = childAV[i];
		child.StandardDeviations[i] = childSD[i];
	}

	// add the variance factor for averages
	for (int i = 0; i <= 3; i++) 
	{
		float variation = 0;
		float variationScaler = FMath::RandRange(0.0f, GenerationVariation);
		bool polarity = FMath::RandBool();
		if (polarity) 
		{
			// positive
			variation = variationScaler * child.Averages[i];
		}
		else 
		{
			//negative
			variation = -1*variationScaler * child.Averages[i];
		}

		// add this variation to the new value
		child.Averages[i] += variation;
	}

	// add the variance factor for SD
	for (int i = 0; i <= 3; i++)
	{
		float variation = 0;
		float variationScaler = FMath::RandRange(0.0f, GenerationVariation);
		bool polarity = FMath::RandBool();
		if (polarity)
		{
			// positive
			variation = variationScaler * child.StandardDeviations[i];
		}
		else
		{
			//negative
			variation = -1 * variationScaler * child.StandardDeviations[i];
		}

		// add this variation to the new value
		child.StandardDeviations[i] += variation;
	}

	return child;
}