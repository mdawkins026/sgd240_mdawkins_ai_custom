// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "BotManager.h"
#include "Bot.generated.h"

UCLASS()
class GENETIC_ALGORITHIM_API ABot : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABot();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Step Function called once per step
	void Step(float xPos, FVector fireDirection, float speed);

	TSubclassOf<class ABall> MyBallBlueprint;

	bool InLoop = false;

	bool Active = false;

	// Timming
	bool IsWaiting = false;

	UPROPERTY(EditAnywhere)
	class USphereComponent* SphereComponent;

	FTimerHandle PauseTimmerHandle;

	FTimerHandle EndingTimmerHandle;

	UFUNCTION()
	void Continue();

	void End();

	// Score (For fitness function)
	int score = 0;

	// Step Number
	int StepNumber = 0;

	// Timing values
	TArray<float> TimmingValues;

	// Xpos Values
	TArray<float> PositionValues;

	// R Vaues
	TArray<float> Rvalues;

	// Theta Values
	TArray<float> ThetaValues;

	void loop();
};
