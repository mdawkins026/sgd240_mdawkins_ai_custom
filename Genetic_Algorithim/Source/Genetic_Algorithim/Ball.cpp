// Fill out your copyright notice in the Description page of Project Settings.


#include "Ball.h"
#include "Goal.h"
#include "Bot.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"

// Sets default values
ABall::ABall()
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ABall::OnHit);		// set up a notification for when this component hits something blocking

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 10.0f;
	ProjectileMovement->MaxSpeed = 10000.0f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();

	Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AGoal::StaticClass(), FoundGoals);
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Notifies the bot manager of specific hits
void ABall::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor == Cast<AActor>(Player)) 
	{
		// Destoy the ball 
		this->Destroy();
		UE_LOG(LogTemp, Warning, TEXT("Deninied XDDDD"));
	}

	else if (FoundGoals.Num() != 0) 
	{
		UE_LOG(LogTemp, Warning, TEXT("The goal was found"));
		for (int i = 0; i < FoundGoals.Num(); i++)
		{
			if (OtherActor == Cast<AActor>(FoundGoals[i]))
			{
				UE_LOG(LogTemp, Warning, TEXT("Hit"));
				FiredFrom->score++;
				this->Destroy();
			}
		}
	}
}

void ABall::FireInDirection(const FVector& ShootDirection, float speed)
{
	ProjectileMovement->Velocity = ShootDirection * speed;
}
