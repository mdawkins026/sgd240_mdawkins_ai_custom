// Fill out your copyright notice in the Description page of Project Settings.

#include "Bot.h"
#include "Ball.h"
#include "Components/SphereComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values
ABot::ABot()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	
	RootComponent = SphereComponent;
	SphereComponent->InitSphereRadius(40.0f);

	static ConstructorHelpers::FObjectFinder<UBlueprint> ItemBlueprint(TEXT("Blueprint'/Game/BallBP.BallBP'"));

	if (ItemBlueprint.Object) {
		MyBallBlueprint = (UClass*)ItemBlueprint.Object->GeneratedClass;
	}
}

// Called when the game starts or when spawned
void ABot::BeginPlay()
{
	Super::BeginPlay();
	InLoop = true;
	Active = true;
}

// Called every frame
void ABot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Main ai loop
	loop();
}

// Called to bind functionality to input
void ABot::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ABot::Step(float xPos, FVector fireDirection, float speed)
{
	// set the location of the bot
	this->SetActorLocation(FVector(-400, xPos, 30.0f));

	// Attempt to fire a projectile.
	UWorld* World = GetWorld();
	if (World)
	{
		// Spawn the projectile at the muzzle.
		ABall* Projectile = World->SpawnActor<ABall>(MyBallBlueprint,this->GetActorLocation(), this->GetActorRotation());
		if (Projectile)
		{
			Projectile->FiredFrom = this;
			Projectile->FireInDirection(fireDirection, speed);
		}
	}
}

void ABot::Continue()
{
	IsWaiting = false;
	UE_LOG(LogTemp, Warning, TEXT("I have stopped waiting :)"));
}

void ABot::End()
{
	// destoy all balls
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABall::StaticClass(), FoundActors);
	for (int i = 0; i < FoundActors.Num(); i++) 
	{
		FoundActors[i]->Destroy();
	}
	Active = false;
}

void ABot::loop()
{
	UE_LOG(LogTemp, Warning, TEXT("The current bot step numebr is %d"), StepNumber);

	// check if the bot is active
	if (InLoop == true)
	{
		// Check that all data is correct
		if ((TimmingValues.Num() == PositionValues.Num())&&( Rvalues.Num() == ThetaValues.Num()) &&(Rvalues.Num()== PositionValues.Num()))
		{
			// Check That the Ai is not waiting
			if (IsWaiting == false)
			{
				FVector direction = FVector(FMath::Cos(FMath::DegreesToRadians(ThetaValues[StepNumber])), FMath::Sin(FMath::DegreesToRadians(ThetaValues[StepNumber])), 0);

				// Ending Condition
				if (StepNumber == (TimmingValues.Num() - 1))
				{
					Step(PositionValues[StepNumber], direction, Rvalues[StepNumber]);
					InLoop = false;
					GetWorld()->GetTimerManager().SetTimer(EndingTimmerHandle, this, &ABot::End, 5.0f, false);
				}

				// All other conditions
				else
				{
					Step(PositionValues[StepNumber], direction, Rvalues[StepNumber]);
					IsWaiting = true;
					if (TimmingValues[StepNumber] > 5) 
					{
						GetWorld()->GetTimerManager().SetTimer(PauseTimmerHandle, this, &ABot::Continue, 5.0f, false);
					}
					else if (TimmingValues[StepNumber] <= 1) 
					{
						GetWorld()->GetTimerManager().SetTimer(PauseTimmerHandle, this, &ABot::Continue, 1.0f, false);
					}
					else 
					{
						GetWorld()->GetTimerManager().SetTimer(PauseTimmerHandle, this, &ABot::Continue, TimmingValues[StepNumber], false);
					}
					UE_LOG(LogTemp, Warning, TEXT("I am waiting "));
					StepNumber++;
				}
			}
		}
	}
}

