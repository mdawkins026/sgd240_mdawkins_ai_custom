// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Genetic_AlgorithimTarget : TargetRules
{
	public Genetic_AlgorithimTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "Genetic_Algorithim" } );
	}
}
