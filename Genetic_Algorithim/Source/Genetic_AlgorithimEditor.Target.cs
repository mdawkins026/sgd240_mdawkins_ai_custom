// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Genetic_AlgorithimEditorTarget : TargetRules
{
	public Genetic_AlgorithimEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "Genetic_Algorithim" } );
	}
}
